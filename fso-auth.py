#!/usr/bin/env python

import sys
import dbus
import gobject

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

pin = ''
gsm_device_iface = None
loop = gobject.MainLoop()

def onDeviceStatus(status):
    print "DeviceStatus:", status
    if status == "alive-sim-locked":
        gsm_device_iface.SetFunctionality("full", True, pin)
    elif status == "alive-sim-ready":
        pass
    elif status == "alive-registered":
        loop.quit()
        
def fso_auth():
    global gsm_device_iface
    global gsm_sim_iface

    bus = dbus.SystemBus()

    usage_obj = bus.get_object("org.freesmartphone.ousaged", "/org/freesmartphone/Usage")
    usage_iface = dbus.Interface(usage_obj, "org.freesmartphone.Usage")

    gsm_device_obj = bus.get_object("org.freesmartphone.ogsmd", "/org/freesmartphone/GSM/Device")
    gsm_device_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Device")

    gsm_device_iface.connect_to_signal('DeviceStatus', onDeviceStatus)

    usage_iface.RequestResource("GSM")

    print "starting the main loop"
    try:
        loop.run()
    except KeyboardInterrupt:
        print "Interrupted"
        loop.quit()

    gsm_device_iface.SetFunctionality("minimal", True, "")
    usage_iface.ReleaseResource("GSM")

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("usage: %s <pin>\n")
        sys.exit(1)

    pin = sys.argv[1]
    fso_auth()
