#!/usr/bin/env python

import dbus
import gobject

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

def onCallStatus(sender = "", *args, **kwargs):
    status = str(args[0])
    if (args[1].has_key("peer")):
        caller = str(args[1]["peer"])
    else:
        caller = "unknown"

    print "onCallStatus"
    print sender
    print status
    print caller
    print

def fso_listen_for_calls():
    bus = dbus.SystemBus()

    usage_obj = bus.get_object("org.freesmartphone.ousaged", "/org/freesmartphone/Usage")
    usage_iface = dbus.Interface(usage_obj, "org.freesmartphone.Usage")
    usage_iface.RequestResource("GSM")

    gsm_device_obj = bus.get_object("org.freesmartphone.ogsmd", "/org/freesmartphone/GSM/Device")
    gsm_device_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Device")
    gsm_call_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Call")

    print "Set Functionality: full"
    gsm_device_iface.SetFunctionality("full", True, "")

    gsm_call_iface.connect_to_signal('CallStatus', onCallStatus)

    print "starting the main loop"
    loop = gobject.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        print "Interrupted"
        loop.quit()

    gsm_device_iface.SetFunctionality("minimal", True, "")
    usage_iface.ReleaseResource("GSM")

if __name__ == "__main__":
    fso_listen_for_calls()
