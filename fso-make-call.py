#!/usr/bin/env python

import sys
import time
import dbus

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

def onCallStatus(sender = "", *args, **kwargs):
    status = str(args[0])
    if (args[1].has_key("peer")):
        caller = str(args[1]["peer"])
    else:
        caller = "unknown"

    print "onCallStatus"
    print sender
    print status
    print caller
    print

def fso_dial(number):
    bus = dbus.SystemBus()

    usage_obj = bus.get_object("org.freesmartphone.ousaged", "/org/freesmartphone/Usage")
    usage_iface = dbus.Interface(usage_obj, "org.freesmartphone.Usage")
    usage_iface.RequestResource("GSM")

    gsm_device_obj = bus.get_object("org.freesmartphone.ogsmd", "/org/freesmartphone/GSM/Device")
    gsm_device_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Device")
    gsm_call_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Call")

    print "Set Functionality: full"
    gsm_device_iface.SetFunctionality("full", True, "")

    gsm_call_iface.connect_to_signal('CallStatus', onCallStatus)

    id = gsm_call_iface.Initiate(number, "voice")
    print "Call id:", id
    time.sleep(30)
    gsm_call_iface.Release(id)

    gsm_device_iface.SetFunctionality("minimal", True, "")
    usage_iface.ReleaseResource("GSM")

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("usage: %s <number>\n")
        sys.exit(1)

    number = sys.argv[1]
    fso_dial(number)
