#!/usr/bin/env python

import dbus
from pprint import pprint

# Taken from telepathy-gabble:
# http://git.collabora.co.uk/?p=user/wjt/telepathy-gabble-wjt.git;a=blob;f=tests/twisted/servicetest.py;h=476c2e171dc7d535f70fa49827892a4cfb5d8402;hb=HEAD#l265
def unwrap(x):
    """Hack to unwrap D-Bus values, so that they're easier to read when
    printed."""

    if isinstance(x, list):
        return map(unwrap, x)

    if isinstance(x, tuple):
        return tuple(map(unwrap, x))

    if isinstance(x, dict):
        return dict([(unwrap(k), unwrap(v)) for k, v in x.iteritems()])

    if isinstance(x, dbus.Boolean):
        return bool(x)

    for t in [unicode, str, long, int, float]:
        if isinstance(x, t):
            return t(x)

    return x

def fso_print_info():
    # Get proxies and add signal handlers
    bus = dbus.SystemBus()
    usage_obj = bus.get_object("org.freesmartphone.ousaged", "/org/freesmartphone/Usage")
    usage_iface = dbus.Interface(usage_obj, "org.freesmartphone.Usage")
    usage_iface.RequestResource("GSM")

    gsm_device_obj = bus.get_object("org.freesmartphone.ogsmd", "/org/freesmartphone/GSM/Device")

    gsm_info_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.Info")
    gsm_device_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Device")
    gsm_sim_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.SIM")
    gsm_network_iface = dbus.Interface(gsm_device_obj, "org.freesmartphone.GSM.Network")

    info = gsm_info_iface.GetInfo()
    pprint(unwrap(info))
    print

    device_status = gsm_device_iface.GetDeviceStatus()
    pprint(unwrap(device_status))
    print

    features = gsm_device_iface.GetFeatures()
    pprint(unwrap(features))
    print

    functionality = gsm_device_iface.GetFunctionality()
    pprint(unwrap(functionality))
    print

    auth_code = gsm_sim_iface.GetAuthCodeRequired()
    pprint(unwrap(auth_code))
    print

    auth_status = gsm_sim_iface.GetAuthStatus()
    pprint(unwrap(auth_status))
    print

    service_center = gsm_sim_iface.GetServiceCenterNumber()
    pprint(unwrap(service_center))
    print

    sim_info = gsm_sim_iface.GetSimInfo()
    pprint(unwrap(sim_info))
    print

    net_status = gsm_network_iface.GetStatus()
    pprint(unwrap(net_status))
    print

    usage_iface.ReleaseResource("GSM")

if __name__ == "__main__":
    fso_print_info()
